import sqlite3


class QrCodeElement:
    def __init__(self, code: str, **kwargs) -> None:
        self.code = code
        self.parent = kwargs.get("parent")
        self.label = kwargs.get("label")
        self.notes = kwargs.get("notes")
        self.expires = kwargs.get("expires")
        self.created = kwargs.get("created")
        self.updated = kwargs.get("updated")

    def to_list(self) -> list[str]:
        return [self.code, self.parent, self.label, self.notes, self.expires, self.created, self.updated]


class QrCoreSqlite:
    def __init__(self, filename: str) -> None:
        self.connection = sqlite3.connect(filename)

    def close(self) -> None:
        self.connection.close()

    def get(self, code: str) -> QrCodeElement:
        pass

    def create(self, elem: QrCodeElement) -> None:
        cur = self.connection.cursor()
        cur.execute(f"INSERT INTO elements VALUES (? ? ? ? ? ? ?)", elem.to_list())
        cur.commit()

    def update(self, qrcode_element: QrCodeElement) -> QrCodeElement:
        pass

    def remove(self, code: str) -> bool:
        pass

    def list(self) -> list[QrCodeElement]:
        pass


class QrCodeCatalog:
    def __init__(self, storage: QrCodeStorage) -> None:
        self.storage = storage

    def get(self):
        pass

    def create(self):
        pass

    def update(self):
        pass

    def remove(self):
        pass

    def list(self):
        pass
