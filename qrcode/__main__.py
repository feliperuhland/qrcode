import argparse


if __name__ == "__main__":
    menu_parse = argparse.ArgumentParser(prog="QrCode Store")
    subparsers = menu_parse.add_subparsers(dest="command")

    list_parser = subparsers.add_parser("list", help="list qrcodes")
    get_parser = subparsers.add_parser("get", help="get qrcodes")
    remove_parser = subparsers.add_parser("remove", help="remove qrcodes")
    create_parser = subparsers.add_parser("create", help="create qrcodes")
    update_parser = subparsers.add_parser("update", help="update qrcodes")

    args = menu_parse.parse_args()
    print(dir(args))
    print(vars(args))
    print(args)
    
    if args.command is None and len(vars(args)) == 1:
        menu_parse.print_help()
