import cv2


def get_qrcode() -> str:
    cap = cv2.VideoCapture(0)
    detector = cv2.QRCodeDetector()

    while True:
        _, img = cap.read()

        data, bbox, _ = detector.detectAndDecode(img)

        if data:
            break

        cv2.imshow("QRCODEscanner", img)
        if cv2.waitKey(1) == ord("q"):
            break

    return str(data)
